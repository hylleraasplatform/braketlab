<img src="https://gitlab.com/hylleraasplatform/braketlab/-/raw/master/graphics/braketlab_logo.png" width = 250px>

## Out-of-the-box outside-of-the-box thinking

BraketLab is a pure Python-code for doing quantum theory in Jupyter Notebooks.

BraketLab is being developed by Audun Skau Hansen (a.s.hansen@kjemi.uio.no) at the Department of Chemistry, Hylleraas Centre for Quantum Molecular Sciences, University of Oslo.

## Installation

Please use `pip install braketlab` to install this module.

